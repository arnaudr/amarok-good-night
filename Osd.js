/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

function Osd()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Receiver
    this.receiver = null;
    this.member = null;

    // Variables for countdown
    this.interval = 0;
    this.countdown = 0;

    // Nice image
    this.image = new QImage(Amarok.Info.scriptPath() + "/icons/sunny_night.png");

    // Timer
    this.timer = new QTimer();
    this.timer.timeout.connect(this, this.timeout);

    Amarok.debug("<- " + arguments.callee.name);
}

Osd.prototype.connect = function(receiver, member)
{
    Amarok.debug("-> Osd." + "connect");

    this.receiver = receiver;
    this.member = member;

    Amarok.debug("<- Osd." + "connect");
}

Osd.prototype.start = function(interval, countdown)
{
    Amarok.debug("-> Osd." + "start");

    this.interval = interval;
    this.countdown = countdown;
    this.timer.start(0);

    Amarok.debug("<- Osd." + "start");
}

Osd.prototype.stop = function()
{
    Amarok.debug("-> Osd." + "stop");

    this.timer.stop();

    Amarok.debug("<- Osd." + "stop");
}

Osd.prototype.isActive = function()
{
    Amarok.debug("-> Osd." + "isActive");

    return this.timer.active;

    Amarok.debug("<- Osd." + "isActive");
}

Osd.prototype.timeout = function()
{
    Amarok.debug("-> Osd." + "timeout");

    var text;

    if (this.countdown != 0) {
	var seconds = this.countdown * this.interval / 1000;
	text = "Sleeping in " + seconds + " seconds...";
    } else {
	text = "Sleeping NOW!";
    }
    Amarok.Window.OSD.setImage(this.image);
    Amarok.Window.OSD.setText(text);
    Amarok.Window.OSD.show();

    if (this.countdown == 0) {
	this.stop();
	this.member.call(this.receiver);
    }

    this.countdown--;
    this.timer.interval = this.interval;

    Amarok.debug("<- Osd." + "timeout");
}
