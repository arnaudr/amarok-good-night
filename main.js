/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

Importer.loadQtBinding("qt.core");
Importer.loadQtBinding("qt.gui");
Importer.loadQtBinding("qt.uitools");

Importer.include("ConfigIO.js");
Importer.include("Gui.js");
Importer.include("Sleep.js");
Importer.include("Command.js");
Importer.include("Pm.js");
Importer.include("Osd.js");

// Default config, passed by reference to every object.
// It should be modified only by:
//  + ConfigIO.read()
//  + Gui.getValues()
var config = {
    "mode": {
	"music": {
	    "enabled": true
	},
	"album": {
	    "enabled": false
	},
	"songs": {
	    "enabled": false,
	    "value": 10
	},
	"timeout": {
	    "enabled": false,
	    "value": 30
	},
	"time": {
	    "enabled": false,
	    "time": QTime.fromString("23:00:00")
	}
    },
    "fadeout": {
	"enabled": false,
	"value": 5
    },
    "sleep": {
	"turnoff": {
	    "enabled": false
	},
	"quit": {
	    "enabled": false
	},
	"energy": {
	    "enabled": false,
	    "index": 0
	}
    }
};



// Get config
ConfigIO.read(config);

// Create the GUI, set values from conf
var gui = new Gui();
gui.dialog.actionGroupBox.disableButton.clicked.connect(disable);
gui.dialog.actionGroupBox.enableButton.clicked.connect(enable);
gui.setValues(config);

// Check for pm capabilities
var pm = new Pm(gui);
pm.check();

// Sleep object
var sleep = new Sleep(gui);
sleep.connect(this, action);

// Turnoff screen
var turnoffTimer = new QTimer();
turnoffTimer.singleShot = true;
turnoffTimer.timeout.connect(this, turnoffTimeout);

// OSD notifications
var osd = new Osd();
osd.connect(this, osdTimeout);

// Amarok tools menu
Amarok.Window.addToolsMenu("GoodNight", "Good Night", Amarok.Info.scriptPath() + "/icons/sunny_night.png");
Amarok.Window.ToolsMenu.GoodNight["triggered()"].connect(Amarok.Window.ToolsMenu.GoodNight, function() { gui.show(sleep.isActive() || osd.isActive()); });



function disable()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Update conf
    gui.getValues(config);

    // Stop OSD
    osd.stop();

    // Stop turnoff timer
    turnoffTimer.stop();

    // Stop sleep
    sleep.stop();

    // Write conf
    ConfigIO.write(config);

    Amarok.debug("<- " + arguments.callee.name);
}

function enable()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Update conf
    gui.getValues(config);

    // Stop OSD
    osd.stop();

    // Stop turnoff timer
    turnoffTimer.stop();

    // Start sleep
    sleep.start(config);

    // Schedule screen turnoff
    if (config.sleep.turnoff.enabled == true) {
	turnoffTimer.start(10 * 1000);
    }

    // Write conf
    ConfigIO.write(config);

    Amarok.debug("<- " + arguments.callee.name);
}

function action()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Invoke sleep action
    sleep.action();

    // If serious action must be done, give time to user to cancel
    if (config.sleep.quit.enabled == true ||
	config.sleep.energy.enabled == true) {
	osd.start(10 * 1000, 3);
    }

    Amarok.debug("<- " + arguments.callee.name);
}

function turnoffTimeout()
{
    Amarok.debug("-> " + arguments.callee.name);

    Command.screenlock();

    Amarok.debug("<- " + arguments.callee.name);
}

function osdTimeout()
{
    Amarok.debug("-> " + arguments.callee.name);

    var quit_amarok = false;

    // Global action
    if (config.sleep.energy.enabled == true) {
	if (config.sleep.energy.index == 0) {
	    Command.suspend();
	} else if (config.sleep.energy.index == 1) {
	    Command.hibernate();
	} else if (config.sleep.energy.index == 2) {
	    quit_amarok = true;
	    Command.shutdown();
	}
    }

    // Amarok action
    if (config.sleep.quit.enabled == true) {
	quit_amarok = true;
    }
    if (quit_amarok == true) {
	Amarok.quitAmarok();
    }

    Amarok.debug("<- " + arguments.callee.name);
}
