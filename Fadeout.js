/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

function Fadeout()
{
    Amarok.debug("-> " + arguments.callee.name);

    // Variables for changing volume
    this.step = 0;
    this.current = 0;

    // Fade-out timer - called every second to adjust volume
    this.timer = new QTimer();
    this.timer.timeout.connect(this, this.timeout);

    Amarok.debug("<- " + arguments.callee.name);
}

Fadeout.prototype.start = function(seconds)
{
    Amarok.debug("-> Fadeout." + "start at " + new Date());

    // Initialize fade-out variables
    this.step = Amarok.Engine.volume / seconds;
    this.current = 0;

    // Start timer
    this.timer.start(1000);

    Amarok.debug("<- Fadeout." + "start");

}

Fadeout.prototype.stop = function()
{
    Amarok.debug("-> Fadeout." + "stop");

    this.timer.stop();

    Amarok.debug("<- Fadeout." + "stop");
}

// Private methods

Fadeout.prototype.timeout = function()
{
//    Amarok.debug("-> Fadeout." + "timeout");

    // Increment value to add
    this.current += this.step;

    // Decrease volume only if value is > 1, since
    // Amarok.Engine.IncreaseVolume paremeter is an integer
    if (this.current >= 1) {
	var round = Math.floor(this.current);
	Amarok.Engine.DecreaseVolume(round);
	this.current -= round;
    }

    // Check if fade-out is finished
    if (Amarok.Engine.volume <= 0) {
	Amarok.debug("Fade-out stopped at " + new Date());
	this.stop();
    }

//    Amarok.debug("<- Fadeout." + "timeout");
}