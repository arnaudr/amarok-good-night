/******************************************************************************/
/* Copyright (C) 2013 El Boulangero                                           */
/*                                                                            */
/* This file is part of the good-night amarok script.                         */
/*                                                                            */
/* good-night is free software: you can redistribute it and/or modify         */
/* it under the terms of the GNU General Public License as published by       */
/* the Free Software Foundation, either version 3 of the License, or          */
/* (at your option) any later version.                                        */
/*                                                                            */
/* good-night is distributed in the hope that it will be useful,              */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with good-night.  If not, see <http://www.gnu.org/licenses/>.        */
/******************************************************************************/

function Mode(gui)
{
    Amarok.debug("-> " + arguments.callee.name);

    this.gui = gui;
    this.receiver = null;
    this.member = null;

    Amarok.debug("<- " + arguments.callee.name);
}

Mode.prototype.connect = function(receiver, member)
{
    Amarok.debug("-> Mode." + "connect");

    this.receiver = receiver;
    this.member = member;
    this.gui.setModeStatus("Enabled ! ");

    Amarok.debug("<- Mode." + "connect");
}

Mode.prototype.disconnect = function()
{
    Amarok.debug("-> Mode." + "disconnect");

    this.receiver = null;
    this.member = null;

    Amarok.debug("<- Mode." + "disconnect");
}

Mode.prototype.signal = function()
{
    Amarok.debug("-> Mode." + "signal");

    this.member.call(this.receiver);

    Amarok.debug("<- Mode." + "signal");
}

Mode.prototype.setStatus = function(message)
{
    Amarok.debug("-> Mode." + "setStatus");

    this.gui.setModeStatus("Enabled ! " + message);

    Amarok.debug("<- Mode." + "setStatus");
}
