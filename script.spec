[Desktop Entry]
Icon=weather-clear-night
Type=script
ServiceTypes=KPluginInfo

Name=Good Night
Comment=Go to sleep with music

X-KDE-PluginInfo-Name=Good Night
X-KDE-PluginInfo-Version=0.2
X-KDE-PluginInfo-Category=Generic
X-KDE-PluginInfo-Author=El Boulangero
X-KDE-PluginInfo-Email=elboulangero@gmail.com
X-KDE-PluginInfo-Depends=Amarok2.6
X-KDE-PluginInfo-License=GPLV3
X-KDE-PluginInfo-EnabledByDefault=false
