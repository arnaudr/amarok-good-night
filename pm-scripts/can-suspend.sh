#!/bin/sh

RET=$(qdbus org.freedesktop.PowerManagement /org/freedesktop/PowerManagement org.freedesktop.PowerManagement.CanSuspend)

if [ "$RET" != "true" ]; then
    exit 1
fi

exit 0
